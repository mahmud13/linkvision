<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'linkvision');

/** MySQL database username */
define('DB_USER', 'homestead');

/** MySQL database password */
define('DB_PASSWORD', 'secret');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'VqxU?n7CfxB d1v^24]{oM&Ah$~%9~FBk;$c#Tx7C(j(s>K_bJFz,f0v!N01-v/2');
define('SECURE_AUTH_KEY',  'PM`Dnr20l._7nX&_V$N![>,<yD2LW@5h5{}/Ars!]9v6gq$rr,(2qo=hiSpmUrOd');
define('LOGGED_IN_KEY',    'dQ4DDbR*6=J+k.[bFQw?qO^fom-7T928IkO})5:(&7z)hWS1)R8UAzg#RNmS&,[m');
define('NONCE_KEY',        '>UlMF`Cg>o?Q?CWI}2x`UE^b[#-#Q1WM]|>ide1!K6}m&R6+l-4H7?.&EbhyWDJ0');
define('AUTH_SALT',        'gKy]+Rl R.WKByG?cm^yKP3L(XpAzP3jz=OG.d0~|ZO_=ar5lJw< >wPE=@X_e>+');
define('SECURE_AUTH_SALT', 'p13Yzcf<c>G,L#voP?X80K=EahIL+5hJ<}W`(}t`bP:  >1<0BMHx-obh)&qH^=K');
define('LOGGED_IN_SALT',   'Wk;]A:7#?aZV_MObVa#5eh}oE^nA-H=Y;O:,b:xt/;^@y[CqS;C^L8C{5uF];=S=');
define('NONCE_SALT',       'ChZPURGSY4O&mp^,P27C6*:0W0q,:|Z1U{3G^<6DMP1pMbGz5JfZ|-]R$6<#f^1.');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
